from django.contrib import admin

# Register your models here.
from .models import Comment, exerciseComment

admin.site.register(Comment)
admin.site.register(exerciseComment)