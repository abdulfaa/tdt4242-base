from rest_framework import generics, mixins, permissions
from rest_framework.filters import OrderingFilter
from comments.models import Comment, Like, exerciseComment
from comments.permissions import IsCommentVisibleToUser
from workouts.permissions import IsOwner, IsReadOnly
from comments.serializers import CommentSerializer, LikeSerializer, ExerciseCommentSerializer

# Create your views here.
class CommentList(
        mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    serializer_class = CommentSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [OrderingFilter]
    ordering_fields = ["timestamp"]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        workout_pk = self.kwargs.get("pk")
        query_select = Comment.objects.none()

        if workout_pk:
            query_select = Comment.objects.filter(workout=workout_pk)
        elif self.request.user:
            """A comment should be visible to the requesting user if any of the following hold:
            - The comment is on a public visibility workout
            - The comment was written by the user
            - The comment is on a coach visibility workout and the user is the workout owner's coach
            - The comment is on a workout owned by the user
            """
            query_select = []
            for i in Comment.objects.all():
                if IsCommentVisibleToUser.has_object_permission(0, self.request, self, i):
                    query_select.append(i)

        return query_select

# Details of comment
class CommentDetail(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        generics.GenericAPIView,
):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = [
        permissions.IsAuthenticated & IsCommentVisibleToUser & (IsOwner | IsReadOnly)
    ]

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


# List of likes
class LikeList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    serializer_class = LikeSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        return Like.objects.filter(owner=self.request.user)


# Details of like
class LikeDetail(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        generics.GenericAPIView,
):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = [permissions.IsAuthenticated]
    _Detail = []

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


# Create your views here.
class ExerciseCommentList(
        mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView
):
    queryset = exerciseComment.objects.all()
    serializer_class = ExerciseCommentSerializer
    filter_backends = [OrderingFilter]
    ordering_fields = ["date_posted"]

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
            
    def perform_create(self, serializer):
       serializer.save(author=self.request.user)

    def get_queryset(self):
        return exerciseComment.objects.all()


# Details of comment
class ExerciseCommentDetail(
        mixins.RetrieveModelMixin,
        mixins.UpdateModelMixin,
        mixins.DestroyModelMixin,
        generics.GenericAPIView,
):
    queryset = exerciseComment.objects.all()
    serializer_class = ExerciseCommentSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)