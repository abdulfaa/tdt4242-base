"""
Tests for the workouts application.
"""
from django.test import TestCase, RequestFactory
from workouts.permissions import *
from users.models import User
from workouts.models import *

# Create your tests here.

class IsOwnerTest(TestCase):

    def setUp(self):
        self.user1 = User.objects.create(username="testUser1")
        self.user2 = User.objects.create(username="testUser2", coach=self.user1)
        self.request = RequestFactory()
        self.is_owner = IsOwner()
    
    def testIsOwner(self):
        request = self.request.get("/")
        request.user = self.user1
        workout = Workout.objects.create(name="Test workout", date="2022-03-11T04:19:00Z", notes="Test notes", owner=self.user1, visibility="PU")
        permission = self.is_owner.has_object_permission(request, None, workout)
        self.assertTrue(permission)


class IsOwnerOfWorkoutTest(TestCase):

    def setUp(self):
        self.user1 = User.objects.create(username="testUser1")
        self.user2 = User.objects.create(username="testUser2", coach=self.user1)
        self.request = RequestFactory()
        self.is_owner_of_workout = IsOwnerOfWorkout()
        self.workout = Workout.objects.create(name="Test workout", date="2022-03-11T04:19:00Z", notes="Test notes", owner=self.user1, visibility="PU")

    def testIsOwnerOfWorkoutObject(self):
        request = self.request.get("/")
        request.user = self.user1

        exercise = Exercise.objects.create(name="Test exercise", description="Test description", unit="reps")
        exercise_instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=5, number=3)

        permission = self.is_owner_of_workout.has_object_permission(request, None, exercise_instance)
        self.assertTrue(permission)

    def testIsOwnerOfWorkoutGet(self):
        request = self.request.get("/")
        request.user = self.user1
        request.data = {"workout", "api/workouts/1/"}
        permission = self.is_owner_of_workout.has_permission(request, None)
        self.assertTrue(permission)
    
    def testIsOwnerOfWorkoutCreate(self):
        request = self.request.post("/")
        request.user = self.user1
        request.data = {'workout': 'api/workouts/1/'}
        permission = self.is_owner_of_workout.has_permission(request, None)
        self.assertTrue(permission)

    def testIsOwnerOfEmptyWorkout(self):
        request = self.request.post("/")
        request.user = self.user1
        request.data = {'workout': None}
        check_permission = self.is_owner_of_workout.has_permission(request, None)
        self.assertFalse(check_permission)


class IsCoachAndVisibleToCoachTest(TestCase):

    def setUp(self):
        self.user1 = User.objects.create(username="testUser1")
        self.user2 = User.objects.create(username="testUser2", coach=self.user1)
        self.request = RequestFactory()
        self.is_coach_and_visible = IsCoachAndVisibleToCoach()
        self.workout = Workout.objects.create(name="Test workout", date="2022-03-11T04:19:00Z", notes="Test notes", owner=self.user2, visibility="PU")

    def testIscoachAndVisibleToCoach(self):
        request = self.request.get("/")
        request.user = self.user1
        check_permission = self.is_coach_and_visible.has_object_permission(request, None, self.workout)
        self.assertTrue(check_permission)


class IsCoachOfWorkoutAndVisibleToCoachTest(TestCase):
    def setUp(self):
        self.user1 = User.objects.create(username="testUser1")
        self.user2 = User.objects.create(username="testUser2", coach=self.user1)
        self.request = RequestFactory()
        self.is_coach_of_workout = IsCoachOfWorkoutAndVisibleToCoach()
        self.workout = Workout.objects.create(name="Test workout", date="2022-03-11T04:19:00Z", notes="Test notes", owner=self.user2, visibility="PU")

    
    def testIsCoachAndIsVisible(self):
        request = self.request.get("/")
        request.user = self.user1
        exercise = Exercise.objects.create(name="Test exercise", description="Test description", unit="reps")
        exercise_instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=5, number=3)
        check_permission = self.is_coach_of_workout.has_object_permission(request, None, exercise_instance)
        self.assertTrue(check_permission)
    

class IsPublicTest(TestCase):
    
    def setUp(self):
        self.user1 = User.objects.create(username="testUser1")
        self.user2 = User.objects.create(username="testUser2", coach=self.user1)
        self.request = RequestFactory()
        self.is_public = IsPublic()
        self.workout = Workout.objects.create(name="Test workout", date="2022-03-11T04:19:00Z", notes="Test notes", owner=self.user2, visibility="PU")

    def testIsAvailableForCoach(self):
        request = self.request.get("/")
        request.user = self.user1
        check_permission = self.is_public.has_object_permission(request, None, self.workout)
        self.assertTrue(check_permission)


        
class IsWorkoutPublicTest(TestCase):
    def setUp(self):
        self.user1 = User.objects.create(username="testUser1")
        self.user2 = User.objects.create(username="testUser2", coach=self.user1)
        self.request = RequestFactory()
        self.is_public_workout = IsWorkoutPublic()
        self.workout = Workout.objects.create(name="Test workout", date="2022-03-11T04:19:00Z", notes="Test notes", owner=self.user2, visibility="PU")

    def testIsVisibleToCoach(self):
        request = self.request.get("/")
        request.user = self.user1
        exercise = Exercise.objects.create(name="Test exercise", description="Test description", unit="reps")
        exercise_instance = ExerciseInstance.objects.create(workout=self.workout, exercise=exercise, sets=5, number=3)
        check_permission = self.is_public_workout.has_object_permission(request, None, exercise_instance)
        self.assertTrue(check_permission)


class IsReadOnlyTest(TestCase):

    def setUp(self):
        self.user1 = User.objects.create(username="testUser1")
        self.user2 = User.objects.create(username="testUser2", coach=self.user1)
        self.request = RequestFactory()
        self.is_read_only = IsReadOnly()
        self.workout = Workout.objects.create(name="Test workout", date="2022-03-11T04:19:00Z", notes="Test notes", owner=self.user2, visibility="PU")

    def testIsVisibleForCoach(self):
        request = self.request.get("/")
        request.user = self.user1
        check_permission = self.is_read_only.has_object_permission(request, None, self.workout)

        self.assertTrue(check_permission)

