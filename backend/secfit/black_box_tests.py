from asyncio import constants
from calendar import timegm
from cgi import test
from hashlib import new
import time
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import string
import random

chrome_driver = '/Users/madsolsennekkoy/Documents/NTNU/TDT4242 - Avansert programvareutviklinig/EX2/driver/chromedriver' #this needs to be changed for other users

class UserRegisterTest(LiveServerTestCase):
    #functions need to start with 'test'!
    def test_register(self):
        #chrome_driver = '/Users/madsolsennekkoy/Documents/NTNU/TDT4242 - Avansert programvareutviklinig/EX2/driver/chromedriver'
        
        selenium = webdriver.Chrome(chrome_driver)

        selenium.get('http://localhost:9090/register.html')

        # get "Create Account" button 
        create_account = selenium.find_element_by_id('btn-create-account')

        # getting the different form elements
        username = selenium.find_element_by_name('username')
        email = selenium.find_element_by_name('email')
        password = selenium.find_element_by_name('password')
        password1 = selenium.find_element_by_name('password1')
        phone_number = selenium.find_element_by_name('phone_number')
        country = selenium.find_element_by_name('country')
        city = selenium.find_element_by_name('city')
        street_address = selenium.find_element_by_name('street_address')

        username.send_keys('t10')
        email.send_keys('t@t.com')
        password.send_keys('1')
        password1.send_keys('1')
        #phone_number.send_keys('5454353')
        phone_number.send_keys('99999999999999999999999999999999999999999999999999') #50 characters
        country.send_keys('N')
        city.send_keys('T')
        street_address.send_keys('G')

        create_account.send_keys(Keys.RETURN) 
        time.sleep(2)
        assert 'View Workouts' in selenium.page_source

        profile_delete_test(selenium)



    def test_edit_exercise(self):
        selenium = webdriver.Chrome(chrome_driver)

        user_login(selenium, 'test', 'test')

        selenium.get('http://localhost:9090/exercise.html?id=3')


        edit_exercise = selenium.find_element_by_id('btn-edit-exercise')
        edit_exercise.click()

        unit = selenium.find_element_by_id('inputUnit')
        duration = selenium.find_element_by_id('inputDuration')
        calories = selenium.find_element_by_id('inputCalories')

        unit.clear()
        duration.clear()
        calories.clear()

        unit.send_keys('R')
        duration.send_keys('-1')
        calories.send_keys('-1')

        time.sleep(2)

        ok_exercise = selenium.find_element_by_id('btn-ok-exercise')
        ok_exercise.click()

        time.sleep(2)

        assert duration.get_attribute("value") == '-1' # should be != and fail the test

        selenium.close()


# Could not figure out how to delete accounts after test, seems to use the "real" db and not test db

def profile_delete_test(selenium):
    time.sleep(3)
    selenium.get('http://localhost:9090/profile.html')

    edit_button = selenium.find_element_by_id('btn-edit-profile')
    edit_button.click()

    delete_button = selenium.find_element_by_id('btn-delete-profile')
    delete_button.click()

    time.sleep(1)

    assert 'Welcome to SecFit!' in selenium.page_source


def user_login(selenium, user, passwd):
    selenium.get('http://localhost:9090/login.html')

    username = selenium.find_element_by_name('username')
    password = selenium.find_element_by_name('password')

    username.send_keys(user)
    password.send_keys(passwd)
    
    time.sleep(1)

    login_button = selenium.find_element_by_id('btn-login')
    login_button.click()

    time.sleep(2)

    assert 'View Workouts' in selenium.page_source
    
    

class DomainTest(LiveServerTestCase):
    def test_2_way_domain(self):
        #defining user1
        user1 = 'user1'
        email1 = 'email1@email1.com'
        password1 = 'pass1'
        repeat_password1 = 'pass1'
        phone1 = '123'
        country1 = 'country1'
        city1 = 'city1'
        street1 = 'street1'

        #defining user2
        user2 = 'user2'
        email2 = 'email2@email2.com'
        password2 = 'pass2'
        repeat_password2 = 'pass2'
        phone2 = '456'
        country2 = 'country2'
        city2 = 'city2'
        street2 = 'street2'


        test_register_verification(user1, email1, password1, repeat_password1, phone1, country1, city1,street1, testnr=1)

        test_register_verification(user1, email2, password2, repeat_password2, phone2, country1, city2, street2,testnr=2)

        test_register_verification(user2, email2, password2, repeat_password1, phone1, country1, city2, street1, testnr=3)

        test_register_verification(user2, email1, password1, repeat_password2, phone2, country1, city1, street2, testnr=4)
        



def test_register_verification(username, email, password, password_repeat, phone_number, country, city, street_address, testnr):
    selenium = webdriver.Chrome(chrome_driver)

    selenium.get('http://localhost:9090/register.html')

    # get "Create Account" button 
    create_account_button = selenium.find_element_by_id('btn-create-account')

    # getting the different form elements
    username_element = selenium.find_element_by_name('username')
    email_element = selenium.find_element_by_name('email')
    password_element = selenium.find_element_by_name('password')
    password_repeat_element = selenium.find_element_by_name('password1')
    phone_number_element = selenium.find_element_by_name('phone_number')
    country_element = selenium.find_element_by_name('country')
    city_element = selenium.find_element_by_name('city')
    street_address_element = selenium.find_element_by_name('street_address')

    # giving elements in form values
    username_element.send_keys(username)
    email_element.send_keys(email)
    password_element.send_keys(password)
    password_repeat_element.send_keys(password_repeat)
    phone_number_element.send_keys(phone_number) 
    country_element.send_keys(country)
    city_element.send_keys(city)
    street_address_element.send_keys(street_address)

    create_account_button.send_keys(Keys.RETURN) 
    time.sleep(2)

    if('Registration failed!' in selenium.page_source):
        print('Account was not created for test number: ', testnr) 
    if('View Workouts' in selenium.page_source):
        print('Account was created for test number: ', testnr)
    
    selenium.close()



class IntegrationTest(LiveServerTestCase):
    # test if the updating the profile will also update workouts 
    # pre-condition: workout for that user exists
    def test_profile(self):
        selenium = webdriver.Chrome(chrome_driver)
        user_login(selenium, 'test1', 'test')

        selenium.get('http://localhost:9090/profile.html')

        # find button and click
        edit_account_button = selenium.find_element_by_id('btn-edit-profile')
        edit_account_button.click()

        # find form element, clear input, and insert new values
        new_value = 'updated_username'
        username_element = selenium.find_element_by_id('username')
        username_element.clear()
        username_element.send_keys(new_value)


        # click update button
        update_account_button = selenium.find_element_by_id('btn-update-profile')
        update_account_button.click()
        
        time.sleep(2)
        selenium.get('http://localhost:9090/workouts.html') 

        assert new_value in selenium.page_source
        #this can be used to verify that the username has been updated
        #selenium.refresh()
        #updated_username_element = selenium.find_element_by_id('username')
        #assert updated_username_element.get_attribute('value') == new_value

        selenium.close()
            
    def test_exercise_comments(self):
        selenium = webdriver.Chrome(chrome_driver)

        comment_text = ''.join(random.choices(string.ascii_lowercase, k = 10))

        user_login(selenium, 'test', 'test') # user needs to be logged in to check profile

        selenium.get('http://localhost:9090/exercise.html?id=1') 

        comment_area_element = selenium.find_element_by_id('comment-area')
        comment_area_element.send_keys(comment_text)

        time.sleep(2)

        comment_post_button = selenium.find_element_by_id('post-comment')
        comment_post_button.send_keys(Keys.RETURN)

        selenium.refresh()
        time.sleep(1)

        assert comment_text in selenium.page_source

        selenium.close()


class FR5Test(LiveServerTestCase):
    def test_workout_details(self):
        selenium = webdriver.Chrome(chrome_driver)

        user_login(selenium, 'test', 'test') # user needs to be logged in to see workouts

        selenium.get('http://localhost:9090/workouts.html')

        all_workouts_button = selenium.find_elements_by_tag_name('a')
        all_workouts_button[11].click()
        time.sleep(1) 
        workouts = selenium.find_elements_by_class_name('col-lg.text-center')
        workouts[1].click()
        time.sleep(1)

        assert 'Name' in selenium.page_source
        assert 'Date/Time' in selenium.page_source
        assert 'Owner' in selenium.page_source
        assert 'Visibility' in selenium.page_source
        assert 'Notes' in selenium.page_source
    
        time.sleep(3)

        selenium.close()

    def test_workout_public(self):
        selenium = webdriver.Chrome(chrome_driver)

        user_login(selenium, 'test1', 'test') # user needs to be logged in to see workouts

        selenium.get('http://localhost:9090/workouts.html') 

        public_workouts_button = selenium.find_elements_by_tag_name('a')
        public_workouts_button[14].click()
        time.sleep(1)

        workouts = selenium.find_elements_by_class_name('col-lg.text-center')
        workouts[1].click()

        visibility_element = selenium.find_element_by_name('visibility')
        assert visibility_element.get_attribute('value') == 'PU'

        time.sleep(2)

        selenium.close()

