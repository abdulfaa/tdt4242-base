from django.test import TestCase
from users.serializers import UserSerializer
from users.models import User
# Create your tests here.

class userSerializerTest(TestCase):
    def setup(self):
        self.user1 = User.objects.create(username="testuser", email="test@test.com")

    def test_validate_password(self):
        password_data = {"password":"test_pass", "password1":"test_pass1"}
        test_serialize = UserSerializer(data=password_data)
        validation = test_serialize.validate_password("test_pass")
        self.assertEqual(validation, "test_pass")

    def test_create_user(self):
        input = {"email":"test@test", 
        "username":"testuser1", 
        "password":"pass", 
        "phone_number":88664455,
        "country":"USA",
        "city":"New York", 
        "street_address":"858 Main Court South Richmond Hill, NY 11419"}

        test_serialize = UserSerializer(data=input)
        self.user1 = test_serialize.create(input)
        self.assertEqual(self.user1.username, input["username"])
        self.assertEqual(self.user1.email, input["email"])
        self.assertEqual(self.user1.country, input["country"])
        self.assertEqual(self.user1.city, input["city"])
        self.assertEqual(self.user1.phone_number, input["phone_number"])
        self.assertEqual(self.user1.street_address, input["street_address"])

