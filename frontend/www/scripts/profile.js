let edit_profile_button = document.querySelector("#btn-edit-profile");
let delete_button = document.querySelector("#btn-delete-profile");
let update_button = document.querySelector('#btn-update-profile');

async function getUser(id) {  //Old solution
  let result = null;
    try {
      //const response = await fetch(`http://localhost:8000/api/users/`);
      let response = await sendRequest("GET", `${HOST}/api/users/${id}/`);

      if (!response.ok) {
        throw new Error(`Error! status: ${response.status}`);
      }
      result = await response.json();
      console.log(result);
      //appendData(result);
      //set_form_data(result);
      //return result;
    } catch (err) {
        console.log("ikke bra");
      console.log(err);
    }
  }

  function appendData(data) {
    let container = document.querySelector("#profile_title")
    container.textContent = data.username;
    /*
    
    container.innerHTML += 'Email: ' + data.email;
    container.innerHTML += 'City: ' + data.city;
    container.innerHTML += 'Country: ' + data.country;
    */
    let table = document.querySelector("#profile");
            let rows = table.querySelectorAll("tr");
            rows[0].querySelectorAll("td")[1].textContent = data.email; 
            rows[1].querySelectorAll("td")[1].textContent = data.phone_number; 
            rows[2].querySelectorAll("td")[1].textContent = data.city;
            rows[3].querySelectorAll("td")[1].textContent = data.country;
            rows[4].querySelectorAll("td")[1].textContent = data.street_address;
            checkCoach(data, rows)
  }

//getUser(2);
//getCurrent();
set_form_data();

function checkCoach(data, form){
  if(data.coach != null){
    coach = getCoach(data.coach, form);
  }else{
    //rows[5].querySelectorAll("td")[1].textContent = "No coach";
    let selector = `input[name="coach"], textarea[name="coach"]`;
    let input = form.querySelector(selector);
    input.value = "No coach";//data[`${key}`]
  }
  

}

async function getCoach(coach, form) {
  try {
    //const response = await fetch(`http://localhost:8000/api/users/`);
    let response = await sendRequest("GET", `${coach}`);

    if (!response.ok) {
      throw new Error(`Error! status: ${response.status}`);
    }
    const coach_data = await response.json();
    //console.log(coach_data.username);
    //rows[5].querySelectorAll("td")[1].textContent = coach_data.username;
    let selector = `input[name="coach"], textarea[name="coach"]`;
    let input = form.querySelector(selector);
    input.value = coach_data.username;
  } catch (err) {
      console.log("ikke bra coach");
    console.log(err);
  }
}



async function set_form_data(){
  let data = await getCurrentUser();
  let form = document.querySelector("#profile_form");
  let formData = new FormData(form);
  for (let key of formData.keys()) { //looping through all keys in html-form
    let selector = `input[name="${key}"], textarea[name="${key}"]`; //for each(username, ...)
    let input = form.querySelector(selector);
    let newVal = data[key]//data[`${key}`]
    input.value = newVal;
    console.log(input.value);
  }
  checkCoach(data, form);
}

function set_edit_form(){
  var f = document.forms['profile_form'];
  for(var i=0,fLen=f.length-4;i<fLen;i++){ //form-lengden er lengre enn data vi viser
    f.elements[i].readOnly = false;
  }  
  edit_profile_button.style.visibility = 'hidden';
  delete_button.className = delete_button.className.replace(" hide", "");
  update_button.className = update_button.className.replace(" hide", "");


}


edit_profile_button.addEventListener("click", set_edit_form);
update_button.addEventListener("click", update_profile);
delete_button.addEventListener("click", delete_user);


function get_profile_form() {
  let form = document.querySelector("#profile_form");

  let formData = new FormData(form);
  let submitForm = new FormData();
  submitForm.append("username", formData.get('username'));
  submitForm.append("email", formData.get('email'));
  submitForm.append("phone_number", formData.get('phone_number'));
  submitForm.append("city", formData.get('city'));
  submitForm.append("country", formData.get('country'));
  submitForm.append("street_address", formData.get('street_address'));
  return submitForm;
}


async function update_profile() {
  let data = await getCurrentUser();
  let submitForm = get_profile_form();
  let response = await sendRequest("PATCH", `${HOST}/api/users/${data.id}/`, submitForm, "");
  if (!response.ok) {
      let data = await response.json();
      let alert = createAlert("Could not update profile!", data);
      document.body.prepend(alert);
  } else {
      location.reload();
  }
}



async function delete_user(){
  let data = await getCurrentUser();
  console.log(data.id);
  let response = await sendRequest("DELETE", `${HOST}/api/users/${data.id}/`);
  if (!response.ok) {
      let data = await response.json();
      let alert = createAlert(`Could not delete profile ${id}!`, data);
      document.body.prepend(alert);
  } else {
      window.location.replace("index.html");
      location.reload();
  }
}
