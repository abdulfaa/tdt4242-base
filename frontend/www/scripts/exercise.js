let cancelButton;
let okButton;
let deleteButton;
let editButton;
let oldFormData;

class MuscleGroup { 
    constructor(type) {
        this.isValidType = false;
        this.validTypes = ["Legs", "Chest", "Back", "Arms", "Abdomen", "Shoulders"]

        this.type = this.validTypes.includes(type) ? type : undefined;
    }

    setMuscleGroupType = (newType) => {
        this.isValidType = false;
        
        if(this.validTypes.includes(newType)){
            this.isValidType = true;
            this.type = newType;
        }
        else{
            alert("Invalid muscle group!");
        }

    }
    
    getMuscleGroupType = () => {
        console.log(this.type, "SWIOEFIWEUFH")
        return this.type;
    }
}

function handleCancelButtonDuringEdit() {
    setReadOnly(true, "#form-exercise");
    document.querySelector("select").setAttribute("disabled", "")
    okButton.className += " hide";
    deleteButton.className += " hide";
    cancelButton.className += " hide";
    editButton.className = editButton.className.replace(" hide", "");

    cancelButton.removeEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-exercise");
    if (oldFormData.has("name")) form.name.value = oldFormData.get("name");
    if (oldFormData.has("description")) form.description.value = oldFormData.get("description");
    if (oldFormData.has("duration")) form.duration.value = oldFormData.get("duration");
    if (oldFormData.has("calories")) form.calories.value = oldFormData.get("calories");
    if (oldFormData.has("muscleGroup")) form.muscleGroup.value = oldFormData.get("muscleGroup");
    if (oldFormData.has("unit")) form.unit.value = oldFormData.get("unit");
    
    const form_fields = ["name", "description", "duration", "calories", "muscleGroup", "unit"]
    for (const field of form_fields) {
        oldFormData.delete(field)
      }
}

function handleCancelButtonDuringCreate() {
    window.location.replace("exercises.html");
}

async function createExercise() {
    document.querySelector("select").removeAttribute("disabled")
    let form = document.querySelector("#form-exercise");
    let formData = new FormData(form);
    let body = {"name": formData.get("name"), 
                "description": formData.get("description"),
                "duration": formData.get("duration"),
                "calories": formData.get("calories"),
                "muscleGroup": formData.get("muscleGroup"), 
                "unit": formData.get("unit")};

    let response = await sendRequest("POST", `${HOST}/api/exercises/`, body);

    if (response.ok) {
        window.location.replace("exercises.html");
    } else {
        let data = await response.json();
        let alert = createAlert("Could not create new exercise!", data);
        document.body.prepend(alert);
    }
}

function handleEditExerciseButtonClick() {
    setReadOnly(false, "#form-exercise");

    document.querySelector("select").removeAttribute("disabled")

    editButton.className += " hide";
    okButton.className = okButton.className.replace(" hide", "");
    cancelButton.className = cancelButton.className.replace(" hide", "");
    deleteButton.className = deleteButton.className.replace(" hide", "");

    cancelButton.addEventListener("click", handleCancelButtonDuringEdit);

    let form = document.querySelector("#form-exercise");
    oldFormData = new FormData(form);
}

async function deleteExercise(id) {
    let response = await sendRequest("DELETE", `${HOST}/api/exercises/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not delete exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        window.location.replace("exercises.html");
    }
}

async function retrieveExercise(id) {
    let response = await sendRequest("GET", `${HOST}/api/exercises/${id}/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve exercise data!", data);
        document.body.prepend(alert);
    } else {
        document.querySelector("select").removeAttribute("disabled")
        let exerciseData = await response.json();
        let form = document.querySelector("#form-exercise");
        let formData = new FormData(form);

        for (let key of formData.keys()) {
            let selector = ( key !== "muscleGroup" ) ? `input[name="${key}"], textarea[name="${key}"]` : `select[name=${key}]`
            let input = form.querySelector(selector);
            let newVal = exerciseData[key];
            input.value = newVal;
        }
        document.querySelector("select").setAttribute("disabled", "")
    }
}

async function updateExercise(id) {
    let form = document.querySelector("#form-exercise");
    let formData = new FormData(form);

    let muscleGroupSelector = document.querySelector("select")
    muscleGroupSelector.removeAttribute("disabled")

    let selectedMuscleGroup = new MuscleGroup(formData.get("muscleGroup"));

    let body = {"name": formData.get("name"), 
                "description": formData.get("description"),
                "duration": formData.get("duration"),
                "calories": formData.get("calories"),
                "muscleGroup": selectedMuscleGroup.getMuscleGroupType(),
                "unit": formData.get("unit")};
    let response = await sendRequest("PUT", `${HOST}/api/exercises/${id}/`, body);

    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert(`Could not update exercise ${id}`, data);
        document.body.prepend(alert);
    } else {
        muscleGroupSelector.setAttribute("disabled", "")
        console.log("geit")
        handleCancelButtonDuringEdit()
    }
}


function addComment(author, text, date, append) {
    /* Taken from https://www.bootdey.com/snippets/view/Simple-Comment-panel#css*/
    let commentList = document.querySelector("#exercise-comment-list");
    let listElement = document.createElement("li");
    listElement.className = "media";
    let commentBody = document.createElement("div");
    commentBody.className = "media-body";
    let dateSpan = document.createElement("span");
    dateSpan.className = "text-muted pull-right me-1";
    let smallText = document.createElement("small");
    smallText.className = "text-muted";

    if (date != "Now") {
        let localDate = new Date(date);
        smallText.innerText = localDate.toLocaleString();
    } else {
        smallText.innerText = date;
    }

    dateSpan.appendChild(smallText);
    commentBody.appendChild(dateSpan);
    
    let strong = document.createElement("strong");
    strong.className = "text-success";
    strong.innerText = author;
    commentBody.appendChild(strong);
    let p = document.createElement("p");
    p.innerHTML = text;

    commentBody.appendChild(strong);
    commentBody.appendChild(p);
    listElement.appendChild(commentBody);

    if (append) {
        commentList.append(listElement);
    } else {
        commentList.prepend(listElement);
    }

}

async function createComment(exerciseId) {
    let commentArea = document.querySelector("#comment-area");
    let content = commentArea.value;
    let body = {exercise: `${HOST}/api/exercises/${exerciseId}/`, content: content};
    let response = await sendRequest("POST", `${HOST}/api/exerciseComments/`, body);
    if (response.ok) {
        addComment(sessionStorage.getItem("username"), content, "Now", false);
    } else {
        let data = await response.json();
        let alert = createAlert("Failed to create comment!", data);
        document.body.prepend(alert);
    }
}

async function retrieveComments(exerciseId) {
    let response = await sendRequest("GET", `${HOST}/api/exerciseComments/`);
    if (!response.ok) {
        let data = await response.json();
        let alert = createAlert("Could not retrieve comments!", data);
        document.body.prepend(alert);
    } else {
        let data = await response.json();
        let comments = data.results;
        for (let comment of comments) {
            let splitArray = comment.exercise.split("/");
            if (splitArray[splitArray.length - 2] == exerciseId) {
                addComment(comment.owner, comment.content, comment.date_posted, true);
            }
        }
    }
}


window.addEventListener("DOMContentLoaded", async () => {
    cancelButton = document.querySelector("#btn-cancel-exercise");
    okButton = document.querySelector("#btn-ok-exercise");
    deleteButton = document.querySelector("#btn-delete-exercise");
    editButton = document.querySelector("#btn-edit-exercise");
    let divExerciseCommentRow = document.querySelector("#div-exercise-comment-row");
    oldFormData = null;
    let postCommentButton = document.querySelector("#post-comment");

    const urlParams = new URLSearchParams(window.location.search);

    // view/edit
    if (urlParams.has('id')) {
        const exerciseId = urlParams.get('id');
        await retrieveComments(exerciseId);
        await retrieveExercise(exerciseId);

        editButton.addEventListener("click", handleEditExerciseButtonClick);
        deleteButton.addEventListener("click", (async (id) => deleteExercise(id)).bind(undefined, exerciseId));
        okButton.addEventListener("click", (async (id) => updateExercise(id)).bind(undefined, exerciseId));
        postCommentButton.addEventListener("click", (async (id) => createComment(id)).bind(undefined, exerciseId));
        divExerciseCommentRow.className = divExerciseCommentRow.className.replace(" hide", "");
    } 
    //create
    else {
        setReadOnly(false, "#form-exercise");

        editButton.className += " hide";
        okButton.className = okButton.className.replace(" hide", "");
        cancelButton.className = cancelButton.className.replace(" hide", "");

        okButton.addEventListener("click", async () => createExercise());
        cancelButton.addEventListener("click", handleCancelButtonDuringCreate);
    }
});
